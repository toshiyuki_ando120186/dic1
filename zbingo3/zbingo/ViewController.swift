//
//  ViewController.swift
//  zbingo
//
//  Created by user149564 on 12/20/18.
//  Copyright © 2018 user149564. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    
    @IBOutlet weak var BidSet: UILabel!
    
    @IBAction func SetButton(_ sender: UIButton) {
        
     let a = Double(BidAmount.text!)
       
        if BidAmount.text!.isEmpty{
            BidSet.isHidden = false
            BidSet.textColor = UIColor.red
            BidSet.text = "Set Bid"
        }
        else{
        let c = Double(a!)
        BidSet.isHidden = false
        BidSet.textColor = UIColor.blue
        BidSet.text = "$ \(c)"
        }
      
    }
    @IBOutlet weak var BidAmount: UITextField!
    
    
    @IBOutlet weak var b1: UIView!
    @IBOutlet weak var b2: UIView!
    @IBOutlet weak var b3: UIView!
    @IBOutlet weak var b4: UIView!
    @IBOutlet weak var b5: UIView!
    
    @IBOutlet weak var i1: UIView!
    @IBOutlet weak var i2: UIView!
    @IBOutlet weak var i3: UIView!
    @IBOutlet weak var i4: UIView!
    @IBOutlet weak var i5: UIView!
    
    @IBOutlet weak var n1: UIView!
    @IBOutlet weak var n2: UIView!
    @IBOutlet weak var n3: UIView!
    @IBOutlet weak var n4: UIView!
    @IBOutlet weak var n5: UIView!
    
    @IBOutlet weak var g1: UIView!
    @IBOutlet weak var g2: UIView!
    @IBOutlet weak var g3: UIView!
    @IBOutlet weak var g4: UIView!
    @IBOutlet weak var g5: UIView!
    
    @IBOutlet weak var o1: UIView!
    @IBOutlet weak var o2: UIView!
    @IBOutlet weak var o3: UIView!
    @IBOutlet weak var o4: UIView!
    @IBOutlet weak var o5: UIView!
    
    
    @IBOutlet weak var stack1: UIStackView!
    @IBOutlet weak var stack2: UIStackView!
    @IBOutlet weak var stack3: UIStackView!
    @IBOutlet weak var stack4: UIStackView!
    @IBOutlet weak var stack5: UIStackView!
    
    @IBOutlet weak var b1Img: UIImageView!
    @IBOutlet weak var b2Img: UIImageView!
    @IBOutlet weak var b3Img: UIImageView!
    @IBOutlet weak var b4Img: UIImageView!
    @IBOutlet weak var b5Img: UIImageView!
    
    @IBOutlet weak var i1Img: UIImageView!
    @IBOutlet weak var i2Img: UIImageView!
    @IBOutlet weak var i3Img: UIImageView!
    @IBOutlet weak var i4Img: UIImageView!
    @IBOutlet weak var i5Img: UIImageView!
    
    @IBOutlet weak var n1Img: UIImageView!
    @IBOutlet weak var n2Img: UIImageView!
    @IBOutlet weak var n3Img: UIImageView!
    @IBOutlet weak var n4Img: UIImageView!
    @IBOutlet weak var n5Img: UIImageView!
    
    @IBOutlet weak var g1Img: UIImageView!
    @IBOutlet weak var g2Img: UIImageView!
    @IBOutlet weak var g3Img: UIImageView!
    @IBOutlet weak var g4Img: UIImageView!
    @IBOutlet weak var g5Img: UIImageView!
    
    @IBOutlet weak var o1Img: UIImageView!
    @IBOutlet weak var o2Img: UIImageView!
    @IBOutlet weak var o3Img: UIImageView!
    @IBOutlet weak var o4Img: UIImageView!
    @IBOutlet weak var o5Img: UIImageView!
    

    
  
    var firstPoi : CGPoint!
    var seconds = 11
    var timer = Timer()
    let button = UIButton()
    var width_cal : CGFloat = 0.0
    var height_cal : CGFloat = 0.0
    let sampleBook = ["00":"3","01":"8","02":"7","03":"14","04":"5","10":"22","11":"26","12":"17","13":"24","14":"29","20":"33","21":"37","22":"100","23":"39","24":"36","30":"50","31":"47","32":"51","33":"60","34":"46","40":"71","41":"66","42":"75","43":"65","44":"72"] as [String : String]
    var result = ["00":"0","01":"0","02":"0","03":"0","04":"0","10":"0","11":"0","12":"0","13":"0","14":"0","20":"0","21":"0","22":"1","23":"0","24":"0","30":"0","31":"0","32":"0","33":"0","34":"0","40":"0","41":"0","42":"0","43":"0","44":"0"] as [String : String]
    
    
    @IBOutlet weak var label: UILabel!
   
    @objc func counter()
    {
        seconds -= 1
   
        if (seconds == 0)
        {
            seconds = 11
            randomButtonIndex1 = Int(arc4random_uniform(75))
            button.setImage(UIImage(named: buttonArray[randomButtonIndex1]), for: .normal) ;
            button.isHidden = false
        }
       
    }
    
    
    override func viewDidLoad() {
        
        self.view.backgroundColor = UIColor.black
        
         randomButtonIndex1 = Int(arc4random_uniform(75))
        width_cal = ( self.view.frame.width - 40 ) / 5.0
        
        height_cal = (self.view.frame.width - 40) * 207.0 / 163.0 / 6.0
   
        self.view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: width_cal ).isActive = true
        button.heightAnchor.constraint(equalToConstant: height_cal).isActive = true
        
        button.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 30).isActive = true
        button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        
        button.setImage(UIImage(named: buttonArray[randomButtonIndex1]), for: .normal) ;
     
        let singlePan = UIPanGestureRecognizer(target: self, action: #selector(self.panGestureHandler(panGesture:)))
        singlePan.cancelsTouchesInView = false

        button.addGestureRecognizer(singlePan)
      
        button.addTarget(self, action: #selector(buttonAction_release),for: .touchUpInside)
        button.addTarget(self, action: #selector(buttonAction_hold),for: .touchDown)
        
        firstPoi = button.center
       
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.counter), userInfo: nil, repeats: true)

        
    }
    
    @objc func buttonAction_hold(sender: UIButton!) {
        firstPoi = button.center
        print("Button hold")
    }
    
    var xx_i : Int = 6
    var yy_j : Int = 6
    var xx_ii : Int = 6
    var yy_jj : Int = 6
    
    @objc func buttonAction_release(sender: UIButton!) {
        print("Button release")
        let bingo_dic_img = ["00":b1Img,"01":b2Img,"02":b3Img,"03":b4Img,"04":b5Img,"10":i1Img,"11":i2Img,"12":i3Img,"13":i4Img,"14":i5Img,"20":n1Img,"21":n2Img,"22":n3Img,"23":n4Img,"24":n5Img,"30":g1Img,"31":g2Img,"32":g3Img,"33":g4Img,"34":g5Img,"40":o1Img,"41":o2Img,"42":o3Img,"43":o4Img,"44":o5Img] as [String : Any]
        let bingo_dic_uiview = ["00":b1,"01":b2,"02":b3,"03":b4,"04":b5,"10":i1,"11":i2,"12":i3,"13":i4,"14":i5,"20":n1,"21":n2,"22":n3,"23":n4,"24":n5,"30":g1,"31":g2,"32":g3,"33":g4,"34":g5,"40":o1,"41":o2,"42":o3,"43":o4,"44":o5] as [String : Any]
        var centerX = sender.center.x
        var centerY = sender.center.y
        
        var mul_x =  ( self.view.frame.width - 40 - centerX ) / width_cal
        
        print(mul_x)
        var mul_y = 0
        
        for i in 0 ..< 5
        {
            for j in 0 ..< 5 {
                if ( (centerX > (20 + width_cal  *  CGFloat(i))) && (centerX < (20 + width_cal * CGFloat(i+1)) )) {
                    if ( (centerY > (100 + height_cal  *  CGFloat(j+1))) && (centerY < (100 + height_cal * CGFloat(j+2)) )) {
                        xx_i = i
                        yy_j = j
                        print(xx_i)
                        print(yy_j)
                        break
                        }
                    }
            }
            
        }
        
        
        if (xx_i == 6) {
            button.center = firstPoi
        }
        else{
        let str = String(xx_i) + String(yy_j)
        
        if ( String(randomButtonIndex1 + 1) == sampleBook[str]) {
        result[str] = "1"
        self.buttonMove(sender: sender, img:  bingo_dic_img[str] as! UIImageView, uiview: bingo_dic_uiview[str] as! UIView)
        }
        else{
            button.center = firstPoi
            }
        }
       
    }
    
    @objc func panGestureHandler(panGesture recognizer: UIPanGestureRecognizer) {
        
        let location = recognizer.location(in: view)
        print(location.x)
        print(location.y)
        button.center = location
        
       
    }
    
    var randomButtonIndex1: Int = 0
    
    let buttonArray = ["bingo1", "bingo2", "bingo3", "bingo4", "bingo5", "bingo6",  "bingo7",  "bingo8", "bingo9", "bingo10",  "bingo11",  "bingo12", "bingo13", "bingo14", "bingo15", "bingo16", "bingo17", "bingo18", "bingo19", "bingo20", "bingo21", "bingo22", "bingo23", "bingo24", "bingo25", "bingo26", "bingo27", "bingo28", "bingo29", "bingo30", "bingo31", "bingo32", "bingo33", "bingo34", "bingo35", "bingo36", "bingo37", "bingo38", "bingo39", "bingo40", "bingo41", "bingo42", "bingo43", "bingo44", "bingo45", "bingo46", "bingo47", "bingo48", "bingo49", "bingo50", "bingo51", "bingo52", "bingo53", "bingo54", "bingo55", "bingo56", "bingo57", "bingo58", "bingo59",  "bingo60", "bingo61", "bingo62", "bingo63", "bingo64", "bingo65", "bingo66", "bingo67", "bingo68", "bingo69", "bingo70", "bingo71", "bingo72", "bingo73", "bingo74", "bingo75"]
 
    
    func buttonMove(sender: UIButton! , img : UIImageView , uiview : UIView) {

        img.image = UIImage(named: buttonArray[randomButtonIndex1])
        uiview.backgroundColor = UIColor.black
        button.isHidden = true
        xx_i = 6
        yy_j = 6
        self.resultSeeing()

    }
    func resultSeeing() {
        let alert = UIAlertController(title: "Bingo! \n Congratulations", message: "It's recommended you start again.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        
        
        if(result["00"] == "1" && result["01"] == "1" && result["02"] == "1" && result["03"] == "1" && result["04"] == "1"){
            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["10"] == "1" && result["11"] == "1" && result["12"] == "1" && result["13"] == "1" && result["14"] == "1"){

            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["20"] == "1" && result["21"] == "1" && result["22"] == "1" && result["23"] == "1" && result["24"] == "1"){

            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["30"] == "1" && result["31"] == "1" && result["32"] == "1" && result["33"] == "1" && result["34"] == "1"){

            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["40"] == "1" && result["41"] == "1" && result["42"] == "1" && result["43"] == "1" && result["44"] == "1"){

            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["00"] == "1" && result["10"] == "1" && result["20"] == "1" && result["30"] == "1" && result["40"] == "1"){

            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["01"] == "1" && result["11"] == "1" && result["21"] == "1" && result["31"] == "1" && result["41"] == "1"){

            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["02"] == "1" && result["12"] == "1" && result["22"] == "1" && result["32"] == "1" && result["42"] == "1"){
            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["03"] == "1" && result["13"] == "1" && result["23"] == "1" && result["33"] == "1" && result["43"] == "1"){
            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["04"] == "1" && result["14"] == "1" && result["24"] == "1" && result["34"] == "1" && result["44"] == "1"){
            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["00"] == "1" && result["11"] == "1" && result["22"] == "1" && result["33"] == "1" && result["44"] == "1"){
            
            self.present(alert, animated: true)
            timer.invalidate()
        }
        else if (result["40"] == "1" && result["31"] == "1" && result["22"] == "1" && result["13"] == "1" && result["04"] == "1"){
            self.present(alert, animated: true)
            timer.invalidate()
        }
    }
  
}
extension UIView{
    var globalPoint : CGPoint? {
        return self.superview?.convert(self.frame.origin, to: nil)
    }

}



 
