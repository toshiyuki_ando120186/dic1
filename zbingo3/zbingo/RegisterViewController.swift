//
//  RegisterViewController.swift
//  zbingo
//
//  Created by user149564 on 1/9/19.
//  Copyright © 2019 user149564. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var Email: UITextField!
    @IBOutlet weak var Password: UITextField!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Email.delegate = self
        self.Password.delegate = self

        // Do any additional setup after loading the view.
    }
    // Touchoutsidethekeyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {self.view.endEditing(true)
    }
    // pressreturn
    func EmailShouldReturn(_ Email: UITextField) -> Bool {
        Email.resignFirstResponder()
        return(true)
    }
    
    
    @IBAction func Register(_ sender: AnyObject) {
        
        FIRAuth.auth()?.createUser(withEmail: Email.text!, password: Password.text!, completion: { (user, error) in
            
            if error != nil{
                print(error!)
            }
            else {
                //sucess
                print("Registration Sucessful!")
                
            }
        }
        
        )
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
